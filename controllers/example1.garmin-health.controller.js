
const asyncHandler = require('express-async-handler');
const crypto = require('crypto')
const OAuth = require('oauth-1.0a')
const request = require('request')
import { key,secret} from '../config/garmin-health';
var moment = require('moment');
import axios from "axios";
var hdInfo_oauth_nonce;
var hdInfo_oauth_signature;
var headerAuthorizationInfo;



//Acquire Unauthorized Request Token and Token Secret
export const acquireUnauthorizedRequestToken = asyncHandler(async (req, res) => {
  const oauth = OAuth({
    consumer: {key: key,secret: secret,},
    signature_method: 'HMAC-SHA1',
    hash_function(base_string, key) {
        return crypto
              .createHmac('sha1', key)
              .update(base_string)
              .digest('base64')
    },
})

const request_data = {
  url: 'https://connectapi.garmin.com/oauth-service/oauth/request_token',
  method: 'POST',
  data: {},
}
request(
  {
      url: request_data.url,
      method: request_data.method,
      headers: oauth.toHeader(oauth.authorize(request_data)),
  },
  async function(error, response, body) {
      if(error)
       return res.json({status:false,message:error})
        const oauth_token = body.split('&')[0];
         headerAuthorizationInfo =(response.request.headers);
         let hdsInfo =(response.request.headers.Authorization);
         const hdsInfoArray1=(hdsInfo.split(',')[1]);
         const hdsInfoArray2=(hdsInfo.split(',')[2]);
          hdInfo_oauth_nonce = (hdsInfoArray1.split('=')[1]);
          hdInfo_oauth_nonce = hdInfo_oauth_nonce.substring(1, hdInfo_oauth_nonce.length - 1); // Get Auth nonce
          hdInfo_oauth_signature = (hdsInfoArray2.split('=')[1]); 
          hdInfo_oauth_signature = hdInfo_oauth_signature.substring(1, hdInfo_oauth_signature.length - 1); // Get Auth signature

        return res.json({ 'success': 'redirect', 'url':`https://connect.garmin.com/oauthConfirm?${oauth_token}`});
  });
})


//Acquire User Access Token and Token Secret

export const acquireUnauthorizedAccessToken = async(req,res) => {
  try{
       if(!req.body.oauth_token)
       return res.json({status:false,message:'oauth_token is required'});
       if(!req.body.oauth_verifier)
       return res.json({status:false,message:'oauth_verifier is required'});
      //headerAuthorizationInfo is a global variable when you had called first step request token that time store header information
       let OAuthHeader = headerAuthorizationInfo.Authorization + `,oauth_verifier=\"${req.body.oauth_verifier}\",oauth_token=\"${req.body.oauth_token}\"`;
     result = await axios.post(`https://connectapi.garmin.com/oauth-service/oauth/access_token`, null,
     {
        "headers": {
          'Authorization': OAuthHeader,
         'Content-Type': 'application/x-www-form-urlencoded',
           }
       }
       
       );
       res.json({result:result})
      }catch(err){
        res.json({Error:err})
      }

       
}
