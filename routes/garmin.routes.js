import express from 'express';
const garminRoute = express.Router();
import * as garminHealthExample1Controllers from '../controllers/example1.garmin-health.controller';
import * as garminHealthExample2Controllers from '../controllers/example2.garmin-health.controller';
garminRoute.route('/request-token').get(garminHealthExample1Controllers.acquireUnauthorizedRequestToken);
garminRoute.route('/access-token').post(garminHealthExample1Controllers.acquireUnauthorizedAccessToken);

// garminRoute.route('/request-token').get(garminHealthExample2Controllers.acquireUnauthorizedRequestToken);
// garminRoute.route('/access-token').post(garminHealthExample2Controllers.acquireUnauthorizedAccessToken);

export default garminRoute;