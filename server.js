// Imported required packages
import express from 'express';
import cookieParser from 'cookie-parser';
import {errorHandle} from './common-middleware/error-middleware';
import garminRoute from './routes/garmin.routes';
import path from 'path';
require('colors');
require('dotenv').config();
const app = express();
app.use(express.json());
app.use(cookieParser());
 app.use('/static', express.static(path.join(__dirname, './public')));
app.use(express.urlencoded({extended:false}));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Origin, Expires, Authorization, Accept, Cache-Control, Pragma");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
})



app.use('/api',garminRoute);
app.use(errorHandle);
app.get('/', (req, res) => {
    return res.end('Api working');
});

// catch 404
app.use((req, res, next) => {
res.status(404).send('<h2 align=center>Page Not Found!</h2>');
});

// Setup for the server port number
const port = process.env.PORT || 4444;
// Staring our express server
const server = app.listen(port, function () {
 console.log(`Server Running in ${process.env.NODE_ENV} Made On Port ${process.env.PORT}`.yellow);
});